import express from 'express';
const app = express()
const port = 3212;

import axios from 'axios';
import qs from 'qs';
import fs from 'fs';
import delay from 'delay';

import PQueue from 'p-queue';

import cron from 'node-cron';
import mysql from 'mysql';


app.use(express.json());
app.use(express.static('public'))
app.use(express.urlencoded({extended: false}));
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,PUT,PATCH,POST,DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


var microsoft_bearer_token = null;
var microsoft_expiry_date = null;


cron.schedule("*/5 * * * * *", async function () {
  if (microsoft_bearer_token  && Date.now() < microsoft_expiry_date) {
    console.log(`Microsoft Token Valid until ${new Date(microsoft_expiry_date)}`);

  }else{
      try {
          let dat = qs.stringify({
              'client_id': 'ea193956-62f3-474a-b5a9-39bd1008f2a8@631e7d36-7aff-428c-a4a0-214dbeff5992',
              'grant_type': 'client_credentials',
              'client_secret': 'eigp6dWmILtTL0OPZsiczOBAWF91V5xoi+rZrVzVfg4=',
              'resource': '00000003-0000-0ff1-ce00-000000000000/hisert.sharepoint.com@631e7d36-7aff-428c-a4a0-214dbeff5992'
          });
          console.log("obtaining perms")
          var data = await axios.post(
              `https://accounts.accesscontrol.windows.net/631e7d36-7aff-428c-a4a0-214dbeff5992/tokens/oAuth/2`,
              dat,
              {}
          );


          microsoft_bearer_token = data.data["access_token"];
          microsoft_expiry_date = data.data["expires_on"]  * 1000;
      } catch (e){
          console.log(e);

      }}
});




var files = [];

async function getData(path, type) {
    try {
        let config = {
            method: 'post',
            maxBodyLength: Infinity,
            url: `https://hisert.sharepoint.com/sites/Intranet_Pliki/_api/web/getFolderByServerRelativeUrl('Shared Documents${path}')?$expand=${type}`,
            headers: {
                'odata': 'verbose',
                'Accept': 'application/json',
                'Authorization': `Bearer ${microsoft_bearer_token}`
            }
        };

        var data = await axios.request(config);
        return data.data;
    }catch (e){
        console.log(e)
    }

}

var tree = [];
var files_tree = [];
async function getFolders(folder, current_path) {
        tree.push((current_path + "/" + folder).toString());
        var data = await getData((current_path + "/" + folder), "Folders")
        if(data["Folders"].length !== 0){
            for(var x in data["Folders"]){
                await getFolders(data["Folders"][x]["Name"], current_path + "/" + folder);
            }
        }
}
async function getFiles(location) {
    try {
        if (location === "") {
            location = "/";
        }
        files_tree[location] = [];
        var data = await getData(location, "Files");
        for (var file of data["Files"]) {
            var name = file["Name"]
            var link = file["LinkingUri"]
            if (link == null) {
                link = "https://hisert.sharepoint.com/" + file["ServerRelativeUrl"];
            }
            var temparr = {"name": name, "link": link};
            files_tree[location].push(temparr)
            //console.log(temparr);
        }
    }catch (e) {
        console.log(e);
    }

}


cron.schedule("*/10 * * * * *", async function () {
    tree = [];
    if(microsoft_bearer_token  && Date.now() < microsoft_expiry_date){
        var bazowe_foldery = await getData("","Folders");
        var bazowe_foldery_formatted = [];
        for(var x in bazowe_foldery["Folders"]){
            if(bazowe_foldery["Folders"][x]["Name"] !== "Forms"){
                bazowe_foldery_formatted.push(bazowe_foldery["Folders"][x]["Name"]);
            }
        }
        for(var x in bazowe_foldery_formatted){
            await getFolders(bazowe_foldery_formatted[x], "")
        }
        files_tree = [];

        await getFiles("");

        for(var loc of tree){
            await getFiles(loc);
        }
        console.log(files_tree)

        //console.log(tree);

    }
});





app.get('/files', async function (req, res) {
    res.json();
});

var connection = mysql.createConnection({
    host     : 'db.hisert.net',
    user     : 'admin',
    password : 'LLW!leow11',
    database : 'interhisert_intranet'
});

app.post('/users', async function(req,res){
    var resuest = "";
    function removeLastInstance(badtext, str) {
        var charpos = str.lastIndexOf(badtext);
        if (charpos<0) return str;
        var ptone = str.substring(0,charpos);
        var pttwo = str.substring(charpos+(badtext.length));
        return (ptone+pttwo);
    }

    for(var x of req.body.users){
        resuest = resuest + '\"' + x + '\",'
    }


    connection.connect();
    resuest = removeLastInstance(",", resuest);
    console.log(`SELECT * FROM pracownicy WHERE pracownicy.uuid IN (${resuest});`);
    var data = await connection.query(`SELECT * FROM pracownicy WHERE pracownicy.uuid IN (${resuest});`, function (error, results, fields) {
        if (error) throw error;
        res.json(results);
    });
});

app.post('/users/data_zatrudnienia', async function(req,res){

    connection.connect();
    console.log(`UPDATE pracownicy SET pracownicy.data_rozpoczecia_wspolpracy = "${req.body.data_zatrudnienia}" WHERE pracownicy.uuid = "${req.body.uuid}";`);
    var data = await connection.query(`UPDATE pracownicy SET pracownicy.data_rozpoczecia_wspolpracy = "${req.body.data_zatrudnienia}" WHERE pracownicy.uuid = "${req.body.uuid}";`, function (error, results, fields) {
        if (error) throw error;
        res.json(results);
    });

});


app.get('/foldery', async function (req, res) {

    var foldery = {
        "Folder":"ilona.groszczyk@hisert.pl",
        "UPLOAD":"marcin.rudzinski@hisert.pl",
        "Dokumenty":"alicja.stepien@hisert.pl",
        "Dokumenty_Umowy_Hisert_Polska":"alicja.stepien@hisert.pl",
        "Dokumenty_Dostawa":"marcin.rudzinski@hisert.pl",
        "Zalando":"pavlo.mishchenko@hisert.pl",
        "KIP":"monika.michalowska@hisert.pl",
        "Hisert":"ogólno dostępny",
        "Import":"wojciech.drobczynski@hisert.pl",
        "Photo":"ilona.groszczyk@hisert.pl",
        "Grafika":"ilona.groszczyk@hisert.pl",
        "IAI":"ilona.groszczyk@hisert.pl",
        "Tabelki_podst":"ilona.groszczyk@hisert.pl",
        "Tabelki_gotowe-EU (Archiwum)":"ilona.groszczyk@hisert.pl",
        "Raporty_Amazon":"ilona.groszczyk@hisert.pl",
        "Plentymarkets":"ilona.groszczyk@hisert.pl",
        "Opisy ":"ilona.groszczyk@hisert.pl",
        "RODO":"monika.michalowska@hisert.pl",
        "Konstrukcja":"iryna.p@hisert.pl",
        "Zakupy":"wojciech.drobczynski@hisert.pl",
        "Produkcja":"iryna.p@hisert.pl"
    }


    res.json(foldery);
});

app.listen(port, () => {
  console.log(`Produkcyjne API nasłuchuje na porcie: ${port}`)

})

process.on('uncaughtException', function (err) {
});
